package empresa;
import java.time.LocalDate;
import java.util.ArrayList;

public class EmpleadoPermanente extends Empleado {
    
    private int hijos;
    private int antiguedad;
    
    public EmpleadoPermanente( String nombre, String direccion, EstadoCivil estadoCivil,
                     LocalDate fechaNacimiento,float sueldoBasico,int antiguedad, int hijos ){
        
        super(nombre,direccion,estadoCivil,fechaNacimiento,sueldoBasico);
        this.hijos = hijos;
        this.antiguedad = antiguedad;
    }
    
    @Override
    public float sueldoBruto(){
            return getSueldoBasico() + asignacionesFamiliares() + antiguedad();
    }
    
    @Override
    protected float obraSocial(){
            return sueldoBruto() * 0.10f + 20 * hijos; 
    }
    
    @Override
    protected float aportesJubilatorios(){
            return sueldoBruto() * 0.15f; 
    }
    
    private float asignacionesFamiliares(){
        return 150 * hijos + asignacionPorConyuge();
    }
    
    private float antiguedad(){
        return 50 * antiguedad;
    }
    
    private float asignacionPorConyuge(){
        switch(getEstadoCivil()){
            case Casado: 
                return 100;
            default:
                return 0;
        }
    }
    
    @Override
    public ArrayList<String> desglosarSueldo() {
        ArrayList<String> desglose = new ArrayList<String>();
        desglose.add("Sueldo Basico           : " + getSueldoBasico());
        desglose.add("Asignaciones Familiares : " + asignacionesFamiliares());
        desglose.add("AntigŁedad              : " + antiguedad());
        desglose.add("Total Bruto             : " + sueldoBruto());
        desglose.add("Retenciones por OS      : " + obraSocial());
        desglose.add("Aportes Jubilatorios    : " + aportesJubilatorios());
        desglose.add("Total Retenciones       : " + retenciones());
        desglose.add("Total Neto              : " + sueldoNeto());
        return desglose;
    }
    
    
 
}