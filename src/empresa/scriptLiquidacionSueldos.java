package empresa;

import static empresa.EstadoCivil.Soltero;
import java.time.LocalDate;

public class scriptLiquidacionSueldos {
    
    public static void main(String[] args) {
        // Instanciación de la empresa
        // (String nombre, long cuit)
        
        Empresa microsoft = new Empresa("Microsoft",30503285789l);
        
        // Instanciacion de empleado permanente
        // (String nombre, String dirección, EstadoCivil estadoCivil,
        // LocalDate fechaNacimiento,float sueldoBasico,
        // int antigüedad, int hijos )
        
        EmpleadoPermanente mati = new EmpleadoPermanente("Matias",
        "Zeballos 5940",Soltero,LocalDate.of(1992,3,24),200,5,0);
        
        // Instanciacion de empleado Temporario
        // ( String nombre, String dirección, EstadoCivil estadoCivil,
        // LocalDate fechaNacimiento,float sueldoBasico, 
        // LocalDate fechaFinDesignación, int horasExtra )
        
        EmpleadoTemporario fede = new EmpleadoTemporario("Federico",
        "Zeballos 5940",Soltero,LocalDate.of(1994,4,02),500,
        LocalDate.of(1992,3,24),5);
        
        // Instanciacion de empleado Contratado
        //( String nombre, String dirección,EstadoCivil estadoCivil,
        // LocalDate fechaNacimiento,float sueldoBasico,int numeroDeContrato,
        // String medioDePago){
        
        EmpleadoContratado nahue = new EmpleadoContratado("Nahuel","Mitre 200",
        Soltero,LocalDate.of(1995,4,02),1000,201,"Efectivo");
        
        // Se agregregan los empleados a la empresa
        
        microsoft.agregarEmpleado(mati);
        microsoft.agregarEmpleado(fede);
        microsoft.agregarEmpleado(nahue);
        
        // Se liquidan los sueldos
        
        microsoft.liquidarSueldos();
        
        // Se muestran los sueldos liquidados:
        
        microsoft.imprimirSueldos();
    }
}
