package empresa;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

/**
 *
 * @author Matias
 */
public abstract class Empleado {
    
    private final String nombre;
    private String direccion;
    private EstadoCivil estadoCivil;
    private final LocalDate fechaNacimiento;
    private float sueldoBasico;
    
    //Constructor de la clase
    public Empleado( String nombre, String direccion, EstadoCivil estadoCivil,
                     LocalDate fechaNacimiento,float sueldoBasico ){
        
        this.nombre = nombre;
        this.direccion = direccion;
        this.estadoCivil = estadoCivil;
        this.fechaNacimiento = fechaNacimiento;
        this.sueldoBasico = sueldoBasico;
    }
    
    public float sueldoNeto() {
        return (this.sueldoBruto() - this.retenciones());
    }
    
    public float retenciones(){
            return this.obraSocial() + this.aportesJubilatorios(); 
    }
    
    public abstract float sueldoBruto();
    protected abstract float obraSocial();
    protected abstract float aportesJubilatorios();
    public abstract ArrayList<String> desglosarSueldo();
    
    public int getEdad(){
            return (Period.between(fechaNacimiento, LocalDate.now()).getYears());    
    }
    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public EstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public float getSueldoBasico() {
        return sueldoBasico;
    }

    }
